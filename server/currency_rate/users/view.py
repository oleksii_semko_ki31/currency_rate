from flask_restful import Resource
from server.currency_rate.models.rate import Rate


class MainContent(Resource):
    """Class for returning all curerrency rates from db"""

    def get(self):
        content = Rate.query.all()

        result = []
        for item in content:
            result.append({
                'id': item.id,
                'date': str(item.date),
                'rate': item.rate,
                'user': item.users.email
            })

        return {'rates': result}
