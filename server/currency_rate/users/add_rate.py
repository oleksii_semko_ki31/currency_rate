from flask_restful import Resource
from flask import request, make_response, jsonify

from server.currency_rate import db
from server.currency_rate.models.rate import Rate
from server.currency_rate.token_required import token_required


class AddRate(Resource):
    """Class for adding new currency rate"""

    @token_required
    def post(self, current_user):
        overload = request.get_json()
        if not overload['date'] or not overload['rate']:
            return make_response(jsonify({'message': 'Bad request'}), 400)

        item = Rate(user_id=current_user.id, date=overload['date'], rate=overload['rate'])

        db.session.add(item)
        db.session.commit()

        return make_response(jsonify({'message': 'Added new rate'}), 201)
