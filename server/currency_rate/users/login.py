from flask_restful import Resource
from server.currency_rate import app
from flask import request, make_response,  jsonify
from werkzeug.security import check_password_hash
import datetime
from server.currency_rate.models.user import User
import jwt


class Login(Resource):
    """Class for checking user in db and returning jwt"""

    def post(self):
        auth = request.get_json()

        if not auth or not auth['email'] or not auth['password']:
            return make_response('Could not authorize', 400)

        user = User.query.filter_by(email=auth['email']).first()

        if user is None:
            return make_response('Could not authorize', 400)

        if check_password_hash(user.password_hash, auth['password']):
            token = jwt.encode({"id": user.id, 'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=10)},
                               app.config['SECRET_KEY'])

            return make_response(jsonify({'token': token.decode('UTF-8')}))

        return make_response('Could not authorize', 400)