from flask_restful import Resource
from flask import request, jsonify, make_response, request
from werkzeug.security import generate_password_hash
from server.currency_rate.models.user import User
from server.currency_rate import db


class Register(Resource):
    """Class for registration user in data base"""

    def post(self):
        auth = request.get_json()

        if not auth or not auth['email'] or not auth['password']:
            return make_response('Could not authorize', 401)

        user = User.query.filter_by(email=auth['email']).first()

        if user is not None:
            return make_response('Bad request', 400)

        hashed_password = generate_password_hash(auth['password'])

        new_user = User(email=auth['email'], password=hashed_password)

        db.session.add(new_user)
        db.session.commit()

        return make_response('New user was created', 201)
