from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_cors import CORS
from server.currency_rate.config import Config

app = Flask(__name__)

app.config.from_object(Config())

db = SQLAlchemy(app)
api = Api(app)
CORS(app)

from server.currency_rate.users.view import MainContent
from server.currency_rate.users.login import Login
from server.currency_rate.users.register import Register
from server.currency_rate.users.add_rate import AddRate

# Routing
api.add_resource(MainContent, '/content')
api.add_resource(Login, '/users/login')
api.add_resource(Register, '/users/register')
api.add_resource(AddRate, '/users/add_rate')
