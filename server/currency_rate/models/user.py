from server.currency_rate import db


class User(db.Model):
    """User model"""

    _tablename_ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, index=True)
    password_hash = db.Column(db.String(128), unique=True)

    def __init__(self, email=None, password=None):
        self.email = email
        self.password_hash = password

    def __repr__(self):
        return '<User %r>' % self.email

