from server.currency_rate import db
from sqlalchemy.orm import validates


class Rate(db.Model):
    """Currency rate model"""

    _tablename_ = 'rate'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, index=True)
    rate = db.Column(db.Float, index=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', name='fk_user_rate'))

    users = db.relationship('User')

    def __init__(self, date=None, rate=None, user_id=None):
        self.date = date
        self.rate = rate
        self.user_id = user_id

    def __repr__(self):
        return '<Rate %r>' % self.rate

    @validates('rate')
    def validate_rate(self, key, rate):
        assert rate.isdigit()
        return rate

