from functools import wraps
from flask import jsonify, request, make_response
from server.currency_rate.models.user import User
from server.currency_rate import app
import jwt


def token_required(f):
    @wraps(f)
    def decorated(self=None, *args, **kwargs):
        token = None

        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']

        if token is None:
            return make_response(jsonify({'message': 'Token is missing'}), 401)

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(id=data['id']).first_or_404()
        except:
            return make_response(jsonify({'message': 'Token is invalid!'}), 401)

        return f(self, current_user, *args, **kwargs)

    return decorated
