import os


class Config(object):
    """
    Base Configuration Class
    Contains all Application Constant
    Defaults
    """
    # Flask settings
    Debug = False
    IS_PRODUCTION = False
    IS_STAGING = False
    TESTING = False
    SECRET_KEY = 'secret_key'

    POSTGRES = {
        'user': os.environ.get('PGUSER'),
        'pw': os.environ.get('PGPASSWORD'),
        'db': os.environ.get('PGDATABASE'),
        'host': os.environ.get('PGHOST'),
        'port': os.environ.get('PGPORT')
    }

    SQLALCHEMY_DATABASE_URI = 'postgresql://%(user)s:%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
    SQLALCHEMY_POOL_RECYCLE = 500
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_POOL_TIMEOUT = 10

    SQLALCHEMY_ECHO = True
