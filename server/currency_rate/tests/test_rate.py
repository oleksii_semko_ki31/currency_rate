import unittest
from werkzeug.security import generate_password_hash, check_password_hash

from server.currency_rate import app, db

CREATE_USER_URL = '/users/register'
TOKEN_URL = '/users/login'
RATE_URL = '/users/add_rate'
GET_RATE_URL = '/content'


class TestRate(unittest.TestCase):
    """Test currency rate Api works correctly"""
    def setUp(self):
        app.config['Testing'] = True
        self.client = app.test_client()
        self.payload = {
            'email': 'test@testemail.com',
            'password': 'testpass',
        }

        self.rate = {
            'date': '2019-12-31',
            'rate': '25',
        }

        with app.app_context():
            db.create_all()

    def test_create_rate_successful(self):
        """Test creating a new rate is successful"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        res = self.client.post(TOKEN_URL, json=self.payload)

        data = res.get_json()
        res = self.client.post(RATE_URL, json=self.rate, headers={'x-access-token': data['token']})

        self.assertEqual(201, res.status_code)

    def test_rate_limited_to_user(self):
        """Test that only authenticated user can create currency rate"""
        res = self.client.post(RATE_URL, json=self.rate)

        self.assertEqual(401, res.status_code)

    def test_create_rate_invalid_token(self):
        """Test that only a user with valid token can create currency rate"""
        res = self.client.post(RATE_URL, json=self.rate, headers={'x-access-token': 'asdqweq1231131412asadzxcqadsq'})

        self.assertEqual(401, res.status_code)

    def test_create_rate_missing_field(self):
        """Test that date and rate are required"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        res = self.client.post(TOKEN_URL, json=self.payload)

        data = res.get_json()
        invalid_rate = {'date': 'test@testemail.com', 'rate': ''}
        res = self.client.post(RATE_URL, json=invalid_rate, headers={'x-access-token': data['token']})

        self.assertEqual(400, res.status_code)

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class TestPublicRateApi(unittest.TestCase):
    """Test that rates are publicly available"""
    def setUp(self):
        app.config['Testing'] = True
        self.client = app.test_client()

        with app.app_context():
            db.create_all()

        payload = {
            'email': 'test@testemail.com',
            'password': 'testpass',
        }
        self.client.post(CREATE_USER_URL, json=payload)
        res = self.client.post(TOKEN_URL, json=payload)

        data = res.get_json()
        self.rate = {'date': '2019-12-31', 'rate': '25'}

        self.client.post(RATE_URL, json=self.rate, headers={'x-access-token': data['token']})
        self.rate['user'] = payload['email']

    def test_rates_output(self):
        """Test that rates are available"""
        res = self.client.get(GET_RATE_URL)

        data = res.get_json()['rates'][0]

        self.assertEqual(200, res.status_code)
        self.assertEqual(self.rate['date'], data['date'])
        self.assertEqual(float(self.rate['rate']), data['rate'])
        self.assertEqual(self.rate['user'], data['user'])

    def tearDown(self):
        db.session.remove()
        db.drop_all()

if __name__ == '__main__':
    unittest.main()































