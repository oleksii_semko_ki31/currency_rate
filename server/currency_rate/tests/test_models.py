import unittest
from werkzeug.security import generate_password_hash, check_password_hash

from server.currency_rate import app, db
from server.currency_rate.models.user import User
from server.currency_rate.models.rate import Rate


class TestModels(unittest.TestCase):
    """Test that models in data base works correctly"""
    def setUp(self):
        app.config['Testing'] = True
        self.client = app.test_client()
        self.email = 'test@testemail.com'
        self.password = 'testpass'

        with app.app_context():
            db.create_all()

    def test_create_user_successful(self):
        """Test creating a new user is successful"""
        hashed_pass = generate_password_hash(self.password)

        user = User(email=self.email, password=hashed_pass)
        db.session.add(user)
        db.session.commit()

        user = User.query.filter_by(email=self.email).first()
        self.assertEqual(user.email, self.email)
        self.assertTrue(check_password_hash(user.password_hash, self.password))

    def test_create_rate_successful(self):
        """Test creating a new rate is successful"""
        hashed_pass = generate_password_hash(self.password)

        user = User(email=self.email, password=hashed_pass)
        db.session.add(user)
        db.session.commit()

        date = '2019-12-31'
        rate = '25'

        new_rate = Rate(user_id=user.id, date=date, rate=rate)
        db.session.add(new_rate)
        db.session.commit()

        new_rate = Rate.query.filter_by(id=new_rate.id).first()
        self.assertEqual(str(new_rate.date), date)
        self.assertEqual(new_rate.rate, float(rate))
        self.assertEqual(new_rate.user_id, user.id)

    def tearDown(self):
        with app.app_context():
            db.session.remove()
            db.drop_all()