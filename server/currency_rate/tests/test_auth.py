import unittest
from werkzeug.security import check_password_hash

from server.currency_rate import app, db
from server.currency_rate.models.user import User

CREATE_USER_URL = '/users/register'
TOKEN_URL = '/users/login'

class TestAuth(unittest.TestCase):
    """Tests user API"""
    def setUp(self):
        app.config['TESTING'] = True
        self.client = app.test_client()
        self.payload = {'email': 'test@testemail.com', 'password': 'testpass'}

        with app.app_context():
            db.create_all()

    def test_create_valid_user_success(self):
        """Test creating using with a valid payload is successful"""
        res = self.client.post(CREATE_USER_URL, json=self.payload)

        self.assertEqual(res.status_code, 201)
        user = User.query.filter_by(email=self.payload['email']).first()
        self.assertTrue(check_password_hash(user.password_hash, self.payload['password']))

    def test_user_exists(self):
        """Test creating a user that already exists fails"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        res = self.client.post(CREATE_USER_URL, json=self.payload)

        self.assertEqual(res.status_code, 400)

    def test_token_for_user(self):
        """Test that a token is created for user"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        res = self.client.post(TOKEN_URL, json=self.payload)

        self.assertIn(b'token', res.data)
        self.assertEqual(res.status_code, 200)

    def test_create_token_invalid_credentials(self):
        """Test that token is not created if invalid credentials are given"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        invalid = {'email': 'test@testemail.com', 'password': 'wrongpass'}
        res = self.client.post(TOKEN_URL, json=invalid)

        self.assertNotIn(b'token', res.data)
        self.assertEqual(res.status_code, 400)

    def test_create_token_without_user(self):
        """Test that token is not created if user doesn't exist"""
        res = self.client.post(TOKEN_URL, json=self.payload)

        self.assertNotIn(b'token', res.data)
        self.assertEqual(res.status_code, 400)

    def test_create_token_missing_field(self):
        """Test that email and password are required"""
        self.client.post(CREATE_USER_URL, json=self.payload)
        missing = {'email': 'test@testemail.com', 'password': ''}
        res = self.client.post(TOKEN_URL, json=missing)

        self.assertNotIn(b'token', res.data)
        self.assertEqual(res.status_code, 400)

    def tearDown(self):
        with app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == '__main__':
    unittest.main()
