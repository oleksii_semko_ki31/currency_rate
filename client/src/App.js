import React, {Component} from 'react';
import {connect} from 'react-redux';

//Actions
import { fetchRates } from './actions/rate.action';
import { userActions } from './actions/user.action';

// CSS
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.handleRegister = this.handleRegister.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.handleNewRate = this.handleNewRate.bind(this);
  }

  componentDidMount() {
    this.props.dispatch(fetchRates());
  }


  //Handling email and password for registration
  handleRegister(event) {
    event.preventDefault();

    const user = {
      'email': event.target.email.value,
      'password': event.target.password.value
    }
    if (user.email && user.password) {
      this.props.dispatch(userActions.register(user));
    }


  }

  //Handeling emai and password for login
  handleLogin(event) {
    event.preventDefault();

    const user = {
      'email': event.target.email.value,
      'password': event.target.password.value
    }
    if (user.email && user.password) {
      this.props.dispatch(userActions.login(user.email, user.password));
    }
  }

  handleLogout(event) {
    event.preventDefault();

    this.props.dispatch(userActions.logout());
  }

  //Handling rate and date
  handleNewRate(event) {
    event.preventDefault();
    const currency_rate = {
      'date': event.target.date.value,
      'rate': event.target.rate.value
    }

    if (currency_rate.date && currency_rate.rate){
      this.props.dispatch(userActions.addRate(currency_rate));
    }
  }

  //Mapping all currency rates
  renderProducts() {
    return this.props.rate.rates.map((i, index) => (
        <div className="rates_list_item" key={index} >
          <p>User: {i.user}</p>
          <p>Date: {i.date}</p>
          <p>Currency rate: {i.rate} UAH / 1 USD</p>
        </div>
    ));
  }


  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Currency rate</h1>
          {this.props.auth.loggedIn && <h1 className="App-title left"><a href="#openModal">new currency rate</a></h1>}
          {!this.props.auth.loggedIn && <h1 className="App-title left"><a href="#register">Register</a></h1>}
          {!this.props.auth.loggedIn && <h1 className="App-title left"><a href="#login">Log in</a></h1>}
          {this.props.auth.loggedIn && <h1 className="App-title left"><a href="#login" onClick={this.handleLogout}>Log out</a></h1>}
        </header>

        {/* Modal window for adding new product */}
        <div id="openModal" className="modalDialog">
          <div>
              <a href="#close" title="Close" className="close">X</a>
              <form onSubmit={this.handleNewRate}>
                <p>Enter date:</p>
                <input  type="date" name="date"/>
                <p>Enter currency rate of 1$ per UAH:</p>
                <input type="number" name="rate"/>
                <button className="btn" type="submit">Submit</button>
              </form>
          </div>
        </div>

        {/* Modal window for registrating */}
        <div id="register" className="modalDialog">
          <div>
            <a href="#close" title="Close" className="close">X</a>
            <h2>Registration</h2>
            <form onSubmit={this.handleRegister}>
              <input type="email" name="email" placeholder="email@email.com" />
              <input type="password" name="password" placeholder="password" />
              <button className="btn" type="submit">Submit</button>
            </form>
          </div>
        </div>

        {/*Modal window for logging*/}
        <div id="login" className="modalDialog">
          <div>
            <a href="#close" title="Close" className="close">X</a>
            <h2>Log in</h2>
            <form onSubmit={this.handleLogin}>
              <input type="email" name="email" placeholder="email@email.com" />
              <input type="password" name="password" placeholder="password" />
              <button className="btn" type="submit">Submit</button>
            </form>
          </div>
        </div>

        <div className="App-rates_list">
          {this.renderProducts()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({...state});
export default connect(mapStateToProps)(App);
