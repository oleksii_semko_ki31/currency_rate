import { authHeader } from '../helpers';

export const userServices = {
  login,
  logout,
  register,
  addRate
};

// Fucntion for saving token in local storage
function login(username, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({'email': username, 'password': password })
  };

  return fetch('/api/users/login', requestOptions)
    .then(handleResponce)
    .then(user => {
      localStorage.setItem('user', JSON.stringify(user));
      return user
    });
}

// Fucntion for deliting token from local storage
function logout() {
  localStorage.removeItem('user');
}

// Function for creating user in DB
function register(user) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  };

  return fetch('/api/users/register', requestOptions)
    .then(handleResponce);
}

function addRate(currency_rate) {
  const {date, rate} = currency_rate;
  console.log(date); 
  const requestOptions = {
    method: 'POST',
    headers: {...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify({'date': date, 'rate': rate})
  };

  return fetch('/api/users/add_rate', requestOptions)
    .then(handleResponce);
  
}

// Function for catching error
function handleResponce(responce) {
  return responce.text().then(text => {
    const data = text && JSON.parse(text);
    if (!responce.ok) {
      if (responce.statuc === 401) {
        logout();
        window.location.reload(true);
      }

      const error = (data && data.message) || responce.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}

