import {combineReducers} from 'redux';

import rate from './rates.reducer';
import regist from './registration.reducer';
import auth from './authentication.reducer';
import alert from './alert.reducer';

export default combineReducers({
  rate,
  regist,
  auth,
  alert
})
