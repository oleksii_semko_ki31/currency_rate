import { userConstants } from '../constants'

export default (state = {}, action) => {
  switch(action.type) {
    case userConstants.REGISTER_REQUEST:
      return { registrating: true }

    case userConstants.REGISTER_SUCCESS:
      return {};

    default:
      return state;
  }
}
