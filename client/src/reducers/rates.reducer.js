import { rateConstants } from '../constants';

const initState = {
  rates: [], 
  error: null,
}

export default (state = initState, action) => {
  switch (action.type) {
    case rateConstants.GET_RATES:
      return state.rates;

    case rateConstants.FETCH_RATES:
      return {
        ...state,
        rates: action.rates
      }

    case rateConstants.FETCH_DATA_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        items: []
      }
    default:
      return state;
  }
}
