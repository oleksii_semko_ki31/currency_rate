// Core dependencies
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

// Application dependencies
import './index.css';
import App from './App';

// Progressive Web App support
import * as serviceWorker from './serviceWorker';

// Redux store
import applicationStore from './registerApplicationStore';

ReactDOM.render(
  <Provider store={applicationStore()}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
