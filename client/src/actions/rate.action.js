import { rateConstants } from '../constants';

//Fetching rates from API using redux-thunk
export const fetchRates = () =>  {
  return dispatch => {
    return fetch("/api/content")
    .then(res => res.json())
    .then(json => {
      dispatch(fetchRatesSuccess(json.rates));
      return json;
    })
    .catch(error => dispatch(fetchFailure(error)));
  };
};

export const fetchRatesSuccess = rates => ({
  type: rateConstants.FETCH_RATES,
  rates
});

export const fetchFailure = error => ({
  type: rateConstants.FETCH_DATA_FAILURE,
  payload: { error }
});

export const getRates = () => ({type: rateConstants.GET_RATES});
