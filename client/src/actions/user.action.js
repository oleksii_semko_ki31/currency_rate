import { userServices } from '../services';
import { alertActions } from '.';
import { history } from '../helpers';
import { userConstants } from '../constants';

export const userActions = {
    login,
    logout,
    register,
    addRate
};

function login(username, password) {
  return dispatch => {
    dispatch(request({ username }));

    userServices.login(username, password)
      .then(
        user => {
          dispatch(success(user))
          history.push('/');
          window.location.reload(true);
        },

        error => {
          dispatch(failure(error));
          dispatch(alertActions.error(error));
        }
      );
  };

  function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
  function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
  function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
};

function logout() {
  userServices.logout();
  history.push('/');
  window.location.reload(true);
  return { type: userConstants.LOGOUT };
}

function register(user) {
  return dispatch => {
    dispatch(request(user));

    userServices.register(user)
      .then(
        user => {
          dispatch(success());
          dispatch(alertActions.success('Registration successful'));
          history.push('/');
          window.location.reload(true)
        },

        error => {
          history.push('/');
          dispatch(failure(error));
          dispatch(alertActions.error(error));
        }
      );
  };

  function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
  function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
  function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

//Function for adding new currency rate
function addRate(currency_rate) {
  return dispatch => {
    userServices.addRate(currency_rate)
      .then(
        () => {
          dispatch(alertActions.success('Adding success'));
          history.push('/');
          window.location.reload(true);
        },
        
        error => {
          dispatch(alertActions.error(error));
        }
      );
  };
}
