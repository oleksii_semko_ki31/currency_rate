In case you have installed docker on your machine just run:

    docker-compose up --build

After that you should paste in your browser that link:

    http://localhost:3050/

You can see all currency rates, but for adding it you have to registrate and log in. After that you can add item into db.

Unless you have installed docker, you have to type the following commands in terminal:
    In case, if you don't have installed postgres on your computer, change branch to develop, clone repo and type in your terminal
    
        sudo apt-get update
        sudo apt-get install postgresql postgresql-contrib libpq-dev
    
    Then you should create an user and a password:
        
        sudo -i -u postgres psql
        ALTER USER postgres WITH ENCRYPTED PASSWORD 'password';
    
    Next, you should create a database:
        
        CREATE DATABASE my_database;
    
    Change folder to a server:

        cd server;
        pip3 install -r requirements.txt
    
    Type:
        python3 manage.py db upgrade
        python3 app.py
        cd..
    
    Then, install packages for frontend:

        cd client
        npm install
    
    And run:
        npm start
    
    Finally, type in your browser:
        
        http://localhost:3000/

In case you are on Windows, I hope God will help you.
